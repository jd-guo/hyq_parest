/*
 * hyqInverseDynamicsIntegration.hpp
 *
 *  Created on: May 10, 2016
 *      Author: depardo
 */

#ifndef HYQINVERSEDYNAMICSINTEGRATION_HPP_
#define HYQINVERSEDYNAMICSINTEGRATION_HPP_

#include <memory>
#include <ds_hyq_robot/extras/definitions.hpp>
#include <ds_hyq_robot/hyqDimensions.hpp>
#include <ds_hyq_robot/hyqDynamics.hpp>
#include <hyqint_tools/hyqIntegration.hpp>

class HyQInverseDynamicsIntegration : public HyQIntegration
{

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	HyQInverseDynamicsIntegration(std::shared_ptr<HyQDynamics> hyq_dyn):HyQIntegration(hyq_dyn)
	{}
  typedef typename hyq::HyQDimensions::state_vector_t state_vector_t;
  typedef typename hyq::HyQDimensions::control_vector_t control_vector_t;



  void SetConstControlInput(const control_vector_t & tau_corrected) {
		tau_corrected_ = tau_corrected;
  }

	virtual void GetControlInput(const state_vector_t &x , const double t , control_vector_t & u_total) override {

    u_total= tau_corrected_;

	}

	private:
  control_vector_t tau_corrected_;

};

#endif
