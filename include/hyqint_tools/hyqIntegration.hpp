/*
 * hyqIntegration.hpp
 *
 *  Created on: Feb 15, 2016
 *      Author: depardo
 */

#ifndef HYQINTEGRATION_HPP_
#define HYQINTEGRATION_HPP_

#include <memory>
#include <dynamical_systems/SystemIntegrationBase.hpp>
#include <ds_hyq_robot/extras/definitions.hpp>
#include <ds_hyq_robot/hyqDimensions.hpp>
#include <ds_hyq_robot/extras/kinematics/hyqKinematics.hpp>
#include <ds_hyq_robot/hyqDynamics.hpp>
#include <ds_hyq_robot/extras/contactForces/hyqGroundReactionForces.hpp>

class HyQIntegration: public SystemIntegrationBase<hyq::HyQDimensions>
{

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	using SystemIntegrationBase<hyq::HyQDimensions>::time_vector_array_t;

	typedef typename hyq::HyQDimensions::state_vector_t state_vector_t;
	typedef typename hyq::HyQDimensions::control_vector_t control_vector_t;
	typedef typename hyq::HyQDimensions::state_vector_array_t state_vector_array_t;
	typedef typename hyq::HyQDimensions::control_vector_array_t control_vector_array_t;
	typedef typename hyq::HyQDimensions::control_feedback_t control_feedback_t;
	typedef typename hyq::HyQDimensions::control_feedback_array_t control_feedback_array_t;

	typedef std::vector<hyq::contact_forces_t, Eigen::aligned_allocator<hyq::contact_forces_t> > contact_force_vector_array_t;
	typedef std::vector<HyQGroundReactionForces , Eigen::aligned_allocator<HyQGroundReactionForces> > ground_reaction_forces_array_t;

	HyQIntegration(std::shared_ptr<HyQDynamics> hyq_dyn):SystemIntegrationBase(std::shared_ptr<DynamicsBase<hyq::HyQDimensions> >(hyq_dyn)){
		forces_trajectory.clear();
	}

	void ClearCustomTrajectories(){
		forces_trajectory.clear();
	}

	void GetHyQsolution(state_vector_array_t & x_sol, control_vector_array_t & u_sol , time_vector_array_t & time, ground_reaction_forces_array_t & forces_sol){
		this->GetSolution(x_sol,u_sol,time);
		forces_sol = forces_trajectory;
	}

	void CustomObserver(const state_vector_t &x_observer  , const double t){
		std::shared_ptr<HyQDynamics> myp = std::static_pointer_cast<HyQDynamics>(this->dynamics_);
		
		 //second pass on the dynamics to be sure we are using the last version of contact forces
		 //GeneralizedCoordinates tau_minus_h_Mqdd;

		//control_vector_t tau;
		 //Not sure if I need to update the sate again,
		//myp->hyqProjectedDynamics_->hyq_state.updateState(x_observer);

		//this->GetControlInput(x_observer,t,tau);

		//myp->hyqProjectedDynamics_->hyq_state.updateState(x_observer);
		 //myp->hyqProjectedDynamics_->systemCDynamics(tau,tau_minus_h_Mqdd);
		 //myp->hyqProjectedDynamics_->UpdateGRF(tau_minus_h_Mqdd);
		 //forces_trajectory.push_back(myp->hyqProjectedDynamics_->hyq_grf);
//		 std::cout << myp->hyqProjectedDynamics_->hyq_grf.ContactForces().transpose() << std::endl;
//		 std::getchar();

	}

	ground_reaction_forces_array_t forces_trajectory;
	HyQKinematics hyqKin;

};


#endif /* HYQINTEGRATION_HPP_ */
