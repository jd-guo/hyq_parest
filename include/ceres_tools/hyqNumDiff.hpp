#ifndef HYQNUMDIFF_HPP_
#define HYQNUMDIFF_HPP_
#endif

#include <memory>
#include <ceres_tools/settings.h>
#include<iit/robots/hyq/link_data_map.h>
#include <iit/robots/hyq/default_dynparams_getter.h>
#include <ds_hyq_robot/extras/definitions.hpp>
#include <ds_hyq_robot/hyqDimensions.hpp>
#include <ds_hyq_robot/extras/kinematics/hyqKinematics.hpp>
#include <ds_hyq_robot/hyqDynamics.hpp>
#include <hyqint_tools/hyqInverseDynamicsIntegration.hpp>
/* This structure calculates the residual for every step from the measured value
 appears in every time step*/

#include <fstream>
#include <vector>

#include <Eigen/StdVector>

#include <cereal/archives/xml.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/Eigen.hpp>

struct hyqNumDiffResidual {
public:
	typedef typename hyq::HyQDimensions::state_vector_t state_vector_t;
	typedef typename hyq::HyQDimensions::control_vector_t control_vector_t;
	typedef typename hyq::HyQDimensions::state_vector_array_t state_vector_array_t;
	typedef typename hyq::HyQDimensions::control_vector_array_t control_vector_array_t;
	typedef typename hyq::HyQDimensions::control_feedback_t control_feedback_t;
	typedef typename hyq::HyQDimensions::control_feedback_array_t control_feedback_array_t;
	typedef typename iit::HyQ::dyn::DefaultParamsGetter Par_getter;
	typedef std::vector<double> time_vector_array_t;

	hyqNumDiffResidual(state_vector_t x_estimator,
			state_vector_t x_estimator_next, control_vector_t tau_measured,
			GeneralizedCoordinates big_q, GeneralizedCoordinates qd,
			double step_size) :
			par_getter(new Par_getter), feet_contact(true), dynamics(
					new HyQDynamics(feet_contact, *par_getter)), x_estimator(
					x_estimator), x_estimator_next(x_estimator_next), tau_measured(
					tau_measured), big_q(big_q), qd(qd), frequency(
					integrationRate), step_size(step_size), TEST_RUN(
					RUNTIME_OBSERVATION), WITH_OFFSETS(ESTIMATE_BIAS) {
	}

	bool operator()(const double* const inertia_parameters,
			const double* const measure_offsets_t,
			const double* const measure_offsets_t_next,
			double* residuals) const {

		//par_getter.report();
		par_getter->refresh(inertia_parameters);
		dynamics->updateInertiaParameters();
		// Add the offset effect of the torque measurement

		control_vector_t tau_corrected;
		for (int i = 0; i < 12; i++) {
			if (WITH_OFFSETS) {
				tau_corrected(i) = tau_measured(i) - *(measure_offsets_t + i);
			} else {
				tau_corrected = tau_measured;
			}
		}

		// Perform inverse projected dynamics
		HyQInverseDynamicsIntegration syst_int(dynamics);

		syst_int.SetConstControlInput(tau_corrected);
		state_vector_t x_initial;
		myHyQkin.hyqconv_->StateVectorFromGeneralizedCoordinates(big_q, qd,
				x_initial);

		syst_int.Integrate(x_initial, frequency, step_size);
		state_vector_array_t x_solution;
		control_vector_array_t u_solution;
		time_vector_array_t t_solution;

		syst_int.GetSolution(x_solution, u_solution, t_solution);
		// Only take the difference from integration, and adds to the old estimation
		state_vector_t delta_x_pred = x_estimator_next
				- (x_estimator + x_solution.back() - x_solution.at(0));
		// Just compare velocity terms

		Eigen::Matrix<double, 6, 1> delta_res;
		delta_res << delta_x_pred.segment<6>(18);
//		delta_res << delta_x_pred.head(6);

		Eigen::Matrix<double, 6, 1> Q_weighting; // Weighting Matrix
		Q_weighting << 1, 1, 1, 1, 1, 1; // yaw is unobservable
		delta_res = Q_weighting.asDiagonal() * delta_res;

		if (x_estimator_next != state_vector_t::Zero()) { // Valid results
			for (int i = 0; i < 6; ++i) {
				residuals[i] = delta_res(i);
			}

			if (WITH_OFFSETS) {
				for (int i = 0; i < 12; ++i) {
					residuals[i + 6] = measure_offsets_t_next[i]
							- measure_offsets_t[i];
				}
			} else {
				for (int i = 0; i < 12; ++i) {
					residuals[i + 6] = measure_offsets_t_next[i]
							- measure_offsets_t[i];
				}
			}
		}

		else {                                 // Invalid estimations
			for (int i = 0; i < 18; ++i) {
				residuals[i] = 0;
			}
		}

		// take only the difference over time

		if (TEST_RUN) {
			std::cout << "\nx_sol: ";
			for (int i = 0; i < 6; ++i) {
				std::cout << (x_solution.back())(i + 18) << ", ";

			}
			std::cout << "\nx_initial: ";
			for (int i = 0; i < 6; ++i) {
				std::cout << x_solution[0](i + 18) << ", ";

			}

			std::cout << "\nx_estimator: ";
			for (int i = 0; i < 6; ++i) {
				std::cout << x_estimator(i + 18) << ", ";

			}

			std::cout << "\nx_estimator_next: ";
			for (int i = 0; i < 6; ++i) {
				std::cout << x_estimator_next(i + 18) << ", ";

			}
			// Perform Intergration of the predicted accelaration
			// get residual, compare the velocity and angular velocity!
			std::cout << "\nCurrent Residuals: ";
			for (int i = 0; i < 6; ++i) {

				std::cout << residuals[i] << ", ";
			}

			{ // we need these brackets to make sure the archive goes out of scope and flushes
				std::cout << step_size << frequency << x_solution.size();
				std::ofstream outXML("/home/jd/data/SmsProj/eigen.xml");
				cereal::XMLOutputArchive archive_o_xml(outXML);
				archive_o_xml(CEREAL_NVP(x_solution), CEREAL_NVP(u_solution),
						CEREAL_NVP(t_solution));

			}

		}
		return true;
	}

	static ceres::CostFunction* Create(const state_vector_t x_estimator,
			const state_vector_t x_estimator_next,
			const control_vector_t tau_measured,
			const GeneralizedCoordinates big_q, const GeneralizedCoordinates qd,
			const double step_size) {

		return (new ceres::NumericDiffCostFunction<hyqNumDiffResidual,
				ceres::CENTRAL, 18, 10, 12, 12>(
				new hyqNumDiffResidual(x_estimator, x_estimator_next,
						tau_measured, big_q, qd, step_size)));
	}

private:
	const std::shared_ptr<Par_getter> par_getter;
	const HyQKinematics myHyQkin;
	const iit::hyq::LegDataMap<bool> feet_contact;
	const std::shared_ptr<HyQDynamics> dynamics;
	const state_vector_t x_estimator;   // 36*1 Array
	const state_vector_t x_estimator_next;   // 36*1 Array
	const GeneralizedCoordinates big_q;
	const control_vector_t tau_measured;
	const GeneralizedCoordinates qd;
	const double frequency;
	const double step_size;
	const bool TEST_RUN;
	const bool WITH_OFFSETS;
};
