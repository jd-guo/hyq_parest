/*
 * settings.h
 *
 *  Created on: Jun 22, 2016
 *      Author: jd
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

// Choice 1 is Baseline methode
// Choice 2 is As-Kinematic Estimator
// Choice 3 is const. Inertia Parameter
// Choice 4 is Full Approach

// General Setup
bool ESTIMATE_BIAS = false; // Estimate random walk of the measurement bias
bool SIMULATION_DATASET = true; // Identify the Dataset in use
double integrationRate = 500; // Depends on the dataset in use. Sim_data: 1000, Noisefree: 500

// Initialize inertia parameters
bool PERFECT_INITIALIZATION = false; // Use the perfect value to initialize for inertia parameters

// Observation
bool RUNTIME_OBSERVATION = false; // Observe Runtime Residuals and parameters
bool RUNTIME_JACOBIAN = false;

// Choose option 1:
 bool FULL_APPROACH = true; // Whether using Baseline Method, choice 1



// For Other approaches:

// Whether using covariance matrices
bool IDENTIFY_COVARIANCE = false;
// Name of result estimation file
char const * CurrentName = "Estimated_test_Askin.mat";

// Initialize further parameters
bool PERFECT_STATE_INITIALIZE = false;
bool CLOSE_INITIALIZE = false;
// Initialize the first state estimation with ground truth
const double initial_state[] = { 0, 0, 0, 0, 0, 0.3, 0, 0, 0, 0, 0, 0 };
// Fix the 4th leg on the location to avoid drifting
const double fixed_foot_pos[] = {-0.3645, -0.324918, 0};


// Initialize the feethold
const bool PERFECT_FEET_INITIALIZE = true;

// Choose option 2:
bool AS_KIN_STATE_ESTIMATOR = false; //Mimic the state estimator only

// Choose option 3 or 4:
bool ESTIMATE_INERTIA_PARAMETERS = true; //Combine Dynamic model including inertia parameter estimation
// FULL APPROACH


// Solver Setup
int num_thread = 4;


// The covariance from the joint measurement influecing the pj forward dynaimcs seen as constant
//const double covariance_q_fd[]=  {0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,
//		0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,
//		0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,
//		0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,
//		0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,
//		0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,            0,
//		0,            0,            0,            0,            0,            0,   4.38953e-10,   1.1628e-11, -5.04706e-12,  1.12657e-13,  1.37624e-11,  2.18183e-11,
//		0,            0,            0,            0,            0,            0,    1.1628e-11,  2.20605e-10, -2.19662e-11,  8.53828e-12,  9.11159e-13,  3.54175e-12,
//		0,            0,            0,            0,            0,            0,  -5.04706e-12, -2.19662e-11,  9.33713e-11, -1.87591e-12, -2.26748e-12, -2.96547e-13,
//		0,            0,            0,            0,            0,            0,   1.12657e-13,  8.53828e-12, -1.87591e-12,  2.98948e-12, -5.99738e-14,  2.30504e-13,
//		0,            0,            0,            0,            0,            0,   1.37624e-11,  9.11159e-13, -2.26748e-12, -5.99738e-14,  3.76074e-12,  1.40229e-12,
//		0,            0,            0,            0,            0,            0,   2.18183e-11,  3.54175e-12, -2.96547e-13,  2.30504e-13,  1.40229e-12,  3.02089e-12};

#endif /* SETTINGS_H_ */
