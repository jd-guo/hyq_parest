#include "hyqProblem.hpp"




// get Functions



const Vector6d hyqProblem::GetStateEstimate(int i) const {
  Vector6d x_estimate_t;
  x_estimate_t << *(state_estimate_ + 12 * i), *(state_estimate_ + 12 * i
      + 1), *(state_estimate_ + 12 * i + 2), *(state_estimate_
      + 12 * i + 3), *(state_estimate_ + 12 * i + 4), *(state_estimate_
      + 12 * i + 5), *(state_estimate_ + 12 * i + 6), *(state_estimate_
      + 12 * i + 7), *(state_estimate_ + 12 * i + 8), *(state_estimate_
      + 12 * i + 9), *(state_estimate_ + 12 * i + 10), *(state_estimate_
      + 12 * i + 11);
  std::cout << x_estimate_t << std::endl;
  return x_estimate_t;
}
