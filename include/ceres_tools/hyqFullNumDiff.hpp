#ifndef HYQFULLNUMDIFF_HPP_
#define HYQFULLNUMDIFF_HPP_
#endif

#include <memory>
#include <ceres_tools/settings.h>
#include <iit/robots/hyq/link_data_map.h>
#include <iit/robots/hyq/default_dynparams_getter.h>
#include <ds_hyq_robot/extras/definitions.hpp>
#include <ds_hyq_robot/hyqDimensions.hpp>
#include <ds_hyq_robot/extras/kinematics/hyqKinematics.hpp>
#include <ds_hyq_robot/extras/kinematics/hyqConstraintJacobian.hpp>
#include <ds_hyq_robot/hyqDynamics.hpp>
#include <hyqint_tools/hyqInverseDynamicsIntegration.hpp>

/* This structure calculates the residual for every step from the measured value
 appears in every time step*/

// For IMU dynamics, kindr implemented
#include <kindr/rotations/RotationEigen.hpp>
#include <kindr/rotations/RotationDiffEigen.hpp>
//#include <ct/rbd/rigidBodyGroup/state/RBDStateDerivative.h>

#include "InverseKinematics.h"

//#include <hyqint_tools/IMUmodelIntegration.hpp>
#include <boost/numeric/odeint.hpp>

#include <fstream>
#include <vector>

#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <unsupported/Eigen/MatrixFunctions>

#include <cereal/archives/xml.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/Eigen.hpp>

struct hyqFullNumDiffResidual {
public:
	typedef typename hyq::HyQDimensions::state_vector_t state_vector_t;
	typedef typename hyq::HyQDimensions::control_vector_t control_vector_t;
	typedef typename hyq::HyQDimensions::state_vector_array_t state_vector_array_t;
	typedef typename hyq::HyQDimensions::control_vector_array_t control_vector_array_t;

	typedef typename iit::HyQ::dyn::DefaultParamsGetter Par_getter;
	typedef std::vector<double> time_vector_array_t;

	typedef Eigen::Matrix<double, 12, 1> FloatingBaseFullState; // Contains also derivative
	typedef Eigen::Matrix<double, 6, 1> Vector6d;
	typedef Eigen::Matrix<double, 3, 1> Vector3d;
	typedef Eigen::Matrix<double, 6, 6> Matrix6d;
	typedef Eigen::Matrix<double, 3, 3> Matrix3d;

	hyqFullNumDiffResidual(Vector6d IMU_measure, control_vector_t tau_measured,
			actuated_joint_coordinates q_now, actuated_joint_coordinates q_next,
			double step_size) :
			par_getter(new Par_getter), feet_contact(true), dynamics(
					new HyQDynamics(feet_contact, *par_getter)), IMU_measure(
					IMU_measure), tau_measured(tau_measured), q_now(q_now), q_next(
					q_next), frequency(integrationRate), step_size(step_size), myInvkin(
					new InverseKinematics), Jac(new HyQExtendedJacobians), myHyQkin(
					new HyQKinematics) {
	}

	bool operator()(const double* const inertia_parameters,
			const double* const foothold_parameters,
			const double* const bias_estimate,
			const double* const bias_estimate_next,
			const double* const state_estimate,
			const double* const state_estimate_next,

			double* residuals) const {

// Initializing all estimates and parameters

		FloatingBaseFullState x_estimate_t;
		x_estimate_t << *(state_estimate), *(state_estimate + 1), *(state_estimate
				+ 2), *(state_estimate + 3), *(state_estimate + 4), *(state_estimate
				+ 5), *(state_estimate + 6), *(state_estimate + 7), *(state_estimate
				+ 8), *(state_estimate + 9), *(state_estimate + 10), *(state_estimate
				+ 11);

		FloatingBaseFullState x_estimate_t_next;
		x_estimate_t_next << *(state_estimate_next), *(state_estimate_next + 1), *(state_estimate_next
				+ 2), *(state_estimate_next + 3), *(state_estimate_next + 4), *(state_estimate_next
				+ 5), *(state_estimate_next + 6), *(state_estimate_next + 7), *(state_estimate_next
				+ 8), *(state_estimate_next + 9), *(state_estimate_next + 10), *(state_estimate_next
				+ 11);

		Vector6d bias_IMU_t;
		bias_IMU_t << *(bias_estimate), *(bias_estimate + 1), *(bias_estimate
				+ 2), *(bias_estimate + 3), *(bias_estimate + 4), *(bias_estimate
				+ 5);

		Vector6d bias_IMU_t_next;
		bias_IMU_t_next << *(bias_estimate_next), *(bias_estimate_next + 1), *(bias_estimate_next
				+ 2), *(bias_estimate_next + 3), *(bias_estimate_next + 4), *(bias_estimate_next
				+ 5);

		control_vector_t bias_tau_t;
		bias_tau_t << *(bias_estimate + 6), *(bias_estimate + 7), *(bias_estimate
				+ 8), *(bias_estimate + 9), *(bias_estimate + 10), *(bias_estimate
				+ 11), *(bias_estimate + 12), *(bias_estimate + 13), *(bias_estimate
				+ 14), *(bias_estimate + 15), *(bias_estimate + 16), *(bias_estimate
				+ 17);

		control_vector_t bias_tau_t_next;
		bias_tau_t_next << *(bias_estimate_next + 6), *(bias_estimate_next + 7), *(bias_estimate_next
				+ 8), *(bias_estimate_next + 9), *(bias_estimate_next + 10), *(bias_estimate_next
				+ 11), *(bias_estimate_next + 12), *(bias_estimate_next + 13), *(bias_estimate_next
				+ 14), *(bias_estimate_next + 15), *(bias_estimate_next + 16), *(bias_estimate_next
				+ 17);

		FeetPositionDataMap feet_pose_inertia;
		for (int i = 0; i < 3; i++) {
			FootPositionVector p_i;
			p_i << *(foothold_parameters + 3 * i), *(foothold_parameters + 3 * i
					+ 1), *(foothold_parameters + 3 * i + 2);
			feet_pose_inertia[i] = p_i;
		}
		for (int i= 0; i<3 ; i++) {
		feet_pose_inertia[3](i) = fixed_foot_pos[i]; // leg-4 fixed on the ground
	}
//		std::cout << "Initialization done \n";
//---------------------------------------------------------------------------------------
//------------------PART 1----------------------Rigid Body Dynamics Model----------------
//------------------x_pred_fd = FD(p_inertial, theta_inertia, tau_measure)

		// Setup variables used for residual calculation from Forward Dynamics
		FloatingBaseFullState delta_x_Fd;
		control_vector_t delta_bias_tau;

		Eigen::Matrix<double, 12, 12> Q_fd_inv =
				Eigen::Matrix<double, 12, 12>::Zero();
		Eigen::Matrix<double, 12, 12> Q_bias_tau_inv = Eigen::Matrix<double, 12,
				12>::Identity();

		// Make the joint angle from inverse kinematics available for Covariance matrix

		if (!AS_KIN_STATE_ESTIMATOR) { // Perform Dynamics involved
			// Step one, transform the foot hold parameter into the body frame,world to base
			FeetPositionDataMap feet_pose_body = FeetPoseWBtransform(
					x_estimate_t, feet_pose_inertia, false);
			// Step two inverse kinematics

			actuated_joint_coordinates q = invkin(feet_pose_body);

			// step three, Inverse instantaneous Kinematics
			actuated_joint_coordinates q_dot = (q_next - q_now) / step_size;

			// Initialize the full state
			state_vector_t x_fullstate_est_t;
			x_fullstate_est_t << x_estimate_t.head(6), q, x_estimate_t.tail(6), q_dot;
			state_vector_t x_fullstate_est(x_fullstate_est_t);
			// Initialize the dynamic

			if (ESTIMATE_INERTIA_PARAMETERS) {
				par_getter->refresh(inertia_parameters);
			} else { // Use perfect inertia parameters instead
				if (SIMULATION_DATASET) {
					double true_parameters[] = { 64.862, 0.0272220, -0.00002350,
							0.042224, 1.688238, 8.665299, 9.243560, -0.028789,
							0.277694, 0.00382 };
					par_getter->refresh(true_parameters);
				} else {
					double true_parameters[] = { 47.376, -0.02230, -0.00010,
							0.03870, 1.209488, 5.5837, 6.056973, 0.00571,
							-0.190812, -0.012668 };
					par_getter->refresh(true_parameters);
				}
			}
			dynamics->updateInertiaParameters();
			// Correct the torque measurement
			control_vector_t tau_corrected;
			if (ESTIMATE_BIAS) {
				tau_corrected = tau_measured - bias_tau_t;
			} else {
				tau_corrected = tau_measured;
			}

			// Perform inverse projected dynamics
			HyQInverseDynamicsIntegration syst_int(dynamics);
			syst_int.SetConstControlInput(tau_corrected);
			syst_int.Integrate(x_fullstate_est, frequency, step_size);

			// Transform results
			FloatingBaseFullState x_pred_Fd;
			x_pred_Fd.head(6) = x_fullstate_est.head(6);
			x_pred_Fd.tail(6) = x_fullstate_est.segment(18, 6);

			// Compute residuals
			delta_x_Fd = x_pred_Fd - x_estimate_t_next;
			delta_bias_tau = bias_tau_t - bias_tau_t_next;

			if (IDENTIFY_COVARIANCE) {
//				// Compute nummeric Jacobian for the covariance Matrix
//				Eigen::Matrix<double, 12, 12> jac_fd_tau;
//				Eigen::Matrix<double, 12, 12> jac_fd_q;
//
//				for (int i = 0; i < 12; i++) {
//					// Re-do both integration and projected forward dynamics
//					// w.r.t tau
//					control_vector_t tauPerturbed(tau_corrected);
//					tauPerturbed(i) += eps;
//					x_fullstate_est << x_estimate_t.head(6), q, x_estimate_t.tail(
//							6), q_dot;
//					syst_int.SetConstControlInput(tauPerturbed);
//					syst_int.Integrate(x_fullstate_est, frequency, step_size);
//					FloatingBaseFullState x_perturbed_Fd;
//					x_perturbed_Fd.head(6) = x_fullstate_est.head(6);
//					x_perturbed_Fd.tail(6) = x_fullstate_est.segment(18, 6);
//					jac_fd_tau.col(i) = (x_pred_Fd - x_perturbed_Fd) / eps;
//				}
//				for (int i = 0; i < 12; i++) {
//					// w.r.t q_now
//					actuated_joint_coordinates qPertubed(q_now);
//					qPertubed(i) += eps;
//					q_dot = (q_next - qPertubed) / step_size;
//					x_fullstate_est << x_estimate_t.head(6), q, x_estimate_t.tail(
//							6), q_dot;
//					syst_int.SetConstControlInput(tau_corrected);
//					syst_int.Integrate(x_fullstate_est, frequency, step_size);
//					FloatingBaseFullState x_perturbed_Fd;
//					x_perturbed_Fd.head(6) = x_fullstate_est.head(6);
//					x_perturbed_Fd.tail(6) = x_fullstate_est.segment(18, 6);
//					jac_fd_q.col(i) = (x_pred_Fd - x_perturbed_Fd) / eps;
//				}
				// Analytical computation of the Jacobian!
				Eigen::Matrix<double, 6, 12> jac_fd_tau_analytic;
				jac_fd_tau_analytic = step_size*(dynamics->hyqProjectedDynamics_->getTauJacobian(x_fullstate_est_t).block<6,12>(0,0));
				if (RUNTIME_JACOBIAN) {
					std::cout << "At state " << x_estimate_t.transpose()
							<< std::endl;
//
//					std::cout << "Forward Dynamics Torque Jacobian: \n";
//					std::cout << jac_fd_tau << std::endl;
					std::cout << "Forward Dynamics Torque Jacobian analytic: \n";
					std::cout << jac_fd_tau_analytic << std::endl;
//					std::cout << "Forward Dynamics Joint Angle Jacobian: \n";
//					std::cout << jac_fd_q << std::endl;
				}
				// The integrated x term is zero!
				// The projected dynamics says nothing about state, only its derivatives
				Eigen::Matrix<double, 12, 12> Q_fd_sqrt = Eigen::Matrix<double,
						12, 12>::Zero();
				Q_fd_sqrt.block<6,6>(6,6) += jac_fd_tau_analytic * Cov_tau
						* (jac_fd_tau_analytic.transpose());

//				Q_fd_sqrt += 4*jac_fd_q*Cov_q_total*(jac_fd_q.transpose());// Jacobian from torque and joint influences

				Q_fd_sqrt.block<3, 3>(6, 6) += Sigma_Process_ang_vel;// Process noise
				Q_fd_sqrt.block<3, 3>(9, 9) += Sigma_Process_vel;// From integration
				Q_fd_sqrt.block<6, 6>(6, 6) =
						Q_fd_sqrt.block<6, 6>(6, 6).sqrt();	// Only the corner invertible
				Q_fd_inv.block<6, 6>(6, 6) =
						Q_fd_sqrt.block<6, 6>(6, 6).inverse();
				if (RUNTIME_JACOBIAN) {
					std::cout << "Forward Dynamics Weighting\n";
					std::cout << Q_fd_inv << std::endl;
				}
				// To be multiplied to the difference vector directly
			} else {
				Q_fd_inv = Eigen::Matrix<double, 12, 12>::Identity();
			}
		} else {
			delta_x_Fd = FloatingBaseFullState::Zero();
			delta_bias_tau = control_vector_t::Zero();
		}

//-------------------------------------------------------------------------------------------------------
//--------------PART 2-------------------IMU dynamics
//----------------x_pred_IMU = IMU(IMU_measure)

		// Setup variables used for residual calculation from IMU model
		FloatingBaseFullState x_pred_IMU = x_estimate_t;
		Vector6d IMU_corrected;
		Eigen::Matrix<double, 6, 6> Cov_imu; // IMU intern noise
		Eigen::Matrix<double, 12, 12> Q_I_inv =
				Eigen::Matrix<double, 12, 12>::Zero();
		Matrix6d Q_bias_IMU_inv = Matrix6d::Identity();

		// Bias Estimation needs to be considered
		if (ESTIMATE_BIAS) {
			IMU_corrected = IMU_measure - bias_IMU_t;
		} else {
			IMU_corrected = IMU_measure;
		}

		kindr::rotations::eigen_impl::LocalAngularVelocityPD measured_local(
				IMU_corrected.head(3));
		kindr::rotations::eigen_impl::EulerAnglesXyzDiffPD eulerAnglesXyzDiff(
				kindr::rotations::eigen_impl::EulerAnglesXyzPD(
						x_estimate_t.head(3)), measured_local);
		{
//			boost::numeric::odeint::runge_kutta4<FloatingBaseFullState, double,
//					FloatingBaseFullState, double,
//					boost::numeric::odeint::vector_space_algebra> stepper;
			boost::numeric::odeint::euler<FloatingBaseFullState, double,
					FloatingBaseFullState, double,
					boost::numeric::odeint::vector_space_algebra> stepper;
			boost::numeric::odeint::integrate_const(stepper,
					[&]
					( const FloatingBaseFullState &x_pred_IMU , FloatingBaseFullState &dxdt , double t ) {
						dxdt.head(3)=eulerAnglesXyzDiff.toImplementation(); // World frame orientation change
						dxdt.tail(3)=IMU_corrected.tail(3);// Body frame velocity change
						dxdt.segment<3>(6)=Vector3d::Zero();// Body frame angular velocity makes no prediction
						dxdt.segment<3>(3)=(GetBWrotation(x_pred_IMU).transpose())*x_pred_IMU.tail(3);// World frame position change
					}, x_pred_IMU, 0.0, step_size, 1 / frequency);
		}

		// -------------------Calculate difference---------------------
		x_pred_IMU.segment < 3 > (6) = IMU_corrected.head(3); // Predicted angular velocity equals measured from last time step!
		FloatingBaseFullState delta_x_IMU = x_pred_IMU - x_estimate_t_next;
		Vector6d delta_bias_IMU = bias_IMU_t - bias_IMU_t_next;

		// Calculate Variance

		if (IDENTIFY_COVARIANCE) {
			// First, nummeric Jacobian between orientation and local angular velocity
			Eigen::Matrix<double, 12, 6> jac_num_imu;
			Eigen::Matrix<double, 6, 6> Cov_imu = Eigen::Matrix<double, 6, 6>::Zero(); // IMU intern noise
			for (int i = 0; i < 6; i++) {
				FloatingBaseFullState x_per_IMU = x_estimate_t;
				Vector6d IMU_pertubed = IMU_corrected;
				IMU_pertubed(i) += eps;

				kindr::rotations::eigen_impl::LocalAngularVelocityPD measured_local_per(
						IMU_pertubed.head(3));
				kindr::rotations::eigen_impl::EulerAnglesXyzDiffPD eulerAnglesXyzDiff_per(
						kindr::rotations::eigen_impl::EulerAnglesXyzPD(
								x_per_IMU.head(3)), measured_local_per);
				{
//					boost::numeric::odeint::runge_kutta4<FloatingBaseFullState, double,
//							FloatingBaseFullState, double,
//							boost::numeric::odeint::vector_space_algebra> stepper;
					boost::numeric::odeint::euler<FloatingBaseFullState, double,
							FloatingBaseFullState, double,
							boost::numeric::odeint::vector_space_algebra> stepper;
					boost::numeric::odeint::integrate_const(stepper,
							[&]
							( const FloatingBaseFullState &x_per_IMU , FloatingBaseFullState &dxdt , double t ) {
								dxdt.head(3)=eulerAnglesXyzDiff_per.toImplementation(); // World frame orientation change
								dxdt.tail(3)=IMU_pertubed.tail(3);// Body frame velocity change
								dxdt.segment<3>(6)=Vector3d::Zero();// Body frame angular velocity makes no prediction
								dxdt.segment<3>(3)=(GetBWrotation(x_per_IMU).transpose())*x_per_IMU.tail(3);// World frame position change
							}, x_per_IMU, 0.0, step_size, 1 / frequency);
				}

				// -------------------Calculate difference---------------------
				x_per_IMU.segment < 3 > (6) = IMU_pertubed.head(3); // Predicted angular velocity equals measured from last time step!
				jac_num_imu.col(i) = (x_per_IMU - x_pred_IMU) / eps;
			}
			Cov_imu.block<3, 3>(0, 0) = Cov_IMU_omega;
			Cov_imu.block<3, 3>(3, 3) = Cov_IMU_acc;
			if (RUNTIME_JACOBIAN) {
				std::cout << "The calcualted nummeric jacobian equals: \n"
						<< jac_num_imu << std::endl;
			}

			Q_I_inv += jac_num_imu * Cov_imu * (jac_num_imu.transpose());
			Q_I_inv.block<3, 3>(0, 0) += Sigma_Process_orient; // Process noise
			Q_I_inv.block<3, 3>(3, 3) += Sigma_Process_pos; // Process noise
//			Q_I_inv.block<3, 3>(6, 6) += Cov_IMU_omega; // Omega covriance stays the same
			Q_I_inv.block<3, 3>(9, 9) += Sigma_Process_vel; // Process noise
//			Q_I_inv.block<3, 3>(9, 9) += step_size * step_size * Cov_IMU_acc; // Ja = stepsize

			Q_I_inv = Q_I_inv.sqrt().inverse();
			if (RUNTIME_JACOBIAN) {
				std::cout << "IMU dynamics Weighting\n";
				std::cout << Q_I_inv << std::endl;
			}

		} else {
			Q_I_inv = Eigen::Matrix<double, 12, 12>::Identity();
		}

//		std::cout << "Part 2 is done \n";
//-------------------------------------------------------------------------------------------------------
//--------------PART 3-------------------measurement error
//---------------------------------------delta_foothold = forwardKin(q_measure,p_inertial)

		// Setup variables needed to calculate the covariance
		Eigen::Matrix<double, 12, 12> R_inv =
				Eigen::Matrix<double, 12, 12>::Zero();
		FloatingBaseFullState delta_feetpos_B; // also 12 Dimensional
		// Step 1 Forward kinematics get the new foot hold in base
		FeetPositionDataMap f_next_mea_B;
		FeetPositionDataMap f_next_mea_W;
		GeneralizedCoordinates big_q;
		big_q << x_estimate_t_next.head(6), q_next;
		myHyQkin->GetFeetPose(big_q, f_next_mea_W, f_next_mea_B);

		FeetPositionDataMap feet_pose_B_next = FeetPoseWBtransform(
				x_estimate_t_next, feet_pose_inertia, false);

		for (int legs = 0; legs < 4; legs++) {

			delta_feetpos_B.segment < 3 > (legs * 3) = feet_pose_B_next[legs]
					- f_next_mea_B[legs];
		}

		// Calculating Covariance Matrix
		if (IDENTIFY_COVARIANCE) {
			GeneralizedCoordinates bigq;
			bigq << x_estimate_t_next.head(6), invkin(feet_pose_B_next);// Inverse kinematics
			Jac->updateJacobians(bigq);		// Update Jacobian from state
			FootJointJacobian Jr;
			for (int legs = 0; legs < 4; legs++) {
				switch (legs) {
				case iit::hyq::LF:
					Jr = Jac->Jr_LF;
					break;
				case iit::hyq::RF:
					Jr = Jac->Jr_RF;
					break;
				case iit::hyq::LH:
					Jr = Jac->Jr_LH;
					break;
				case iit::hyq::RH:
					Jr = Jac->Jr_RH;
					break;
				}
				R_inv.block<3, 3>(legs * 3, legs * 3) = Jr * Cov_q
						* (Jr.transpose()) + Cov_foothold; // Block diagonal
				if (RUNTIME_JACOBIAN) {
					std::cout << "Jacobian Matrix for the Kin Measurement: \n";
					std::cout << Jr << std::endl;
				}
			}
			R_inv = R_inv.sqrt().inverse();
			// Only Testing the jacobian with the one from nummeric

			if (RUNTIME_JACOBIAN) {
//   				Nummeric calculation of the error term for testing
//					Eigen::Matrix<double,12,12> jac_num_mea;
//					for ( int i = 0; i < 12; i ++){
//						actuated_joint_coordinates q_pertubed=q_next;
//						q_pertubed(i) += eps;
//						GeneralizedCoordinates big_q;
//						big_q << x_estimate_t_next.head(6), q_pertubed;
//						FeetPositionDataMap f_next_mea_B_pert;
//						myHyQkin->GetFeetPose(big_q, f_next_mea_W, f_next_mea_B_pert);
//					for (int legs = 0; legs < 4; legs++) {
//
//					jac_num_mea.col(i).segment<3>(legs*3) = (f_next_mea_B[legs] - f_next_mea_B_pert[legs])/eps;
//				}
//
//				}
//
//				std::cout << "Nummerical Jacobian Matrix for the Kin Measurement: \n";
//				std::cout << jac_num_mea << std::endl;

				std::cout << "At state " << x_estimate_t_next.transpose()
						<< std::endl;
				std::cout << "Weighting Matrix for the Kin Measurement: \n";
				std::cout << R_inv << std::endl;
			}
		} else {
			R_inv = Eigen::Matrix<double, 12, 12>::Identity();
		}

//		std::cout << "Part 3 is done \n";
//-------------------------------------------------------------------------------------------------------
//--------------PART 4------------------Calculate Residual-----------------------------------------------

		// TODO bias term,

		// Assign Residuals
		FloatingBaseFullState delta_zeta_weighted = R_inv * delta_feetpos_B;
		FloatingBaseFullState delta_x_fd_weighted = Q_fd_inv * delta_x_Fd;
		FloatingBaseFullState delta_x_I_weighted = Q_I_inv * delta_x_IMU;
		control_vector_t delta_bias_tau_weighted = Q_bias_tau_inv
				* delta_bias_tau;
		Vector6d delta_bias_IMU_weighted = Q_bias_IMU_inv * delta_bias_IMU;

		for (int i = 0; i < 12; i++) {
			residuals[i] = delta_zeta_weighted(i);
		}

		for (int i = 0; i < 12; i++) {
			residuals[i + 12] = delta_x_fd_weighted(i);
		}
		for (int i = 0; i < 12; i++) {
			residuals[i + 24] = delta_x_I_weighted(i);
		}
		for (int i = 0; i < 12; i++) {
			residuals[i + 36] = delta_bias_tau_weighted(i);
		}
		for (int i = 0; i < 6; i++) {
			residuals[i + 48] = delta_bias_IMU_weighted(i);
		}

		if (RUNTIME_OBSERVATION) {
			ObserveRuntimeResidual(residuals);
			ObserveRuntimeParameter(inertia_parameters, foothold_parameters);
		}
//		std::cout << "Part 4 is done \n";
//--------------------------------------------FINISH Calculating Residuals-------------------------------
		return true;
	}

	static ceres::CostFunction* Create(const Vector6d IMUmeasure,
			const control_vector_t tau_measured,
			const actuated_joint_coordinates q_now,
			const actuated_joint_coordinates q_next, const double step_size) {

		return (new ceres::NumericDiffCostFunction<hyqFullNumDiffResidual,
				ceres::CENTRAL, 54, 10, 12, 18, 18, 12, 12>(
				new hyqFullNumDiffResidual(IMUmeasure, tau_measured, q_now,
						q_next, step_size)));
	}

	FeetPositionDataMap FeetPoseWBtransform(
			const FloatingBaseFullState x_estimate_t,
			const FeetPositionDataMap feet_pose_inertia, bool reverse) const {
		Matrix3d R_wb;
		FeetPositionDataMap feet_pose_body;
		R_wb = Eigen::AngleAxisd(x_estimate_t(coordinates_order::qb2),
				Vector3d::UnitZ())
				* Eigen::AngleAxisd(x_estimate_t(coordinates_order::qb1),
						Vector3d::UnitY())
				* Eigen::AngleAxisd(x_estimate_t(coordinates_order::qb0),
						Vector3d::UnitX());

		for (int legs = 0; legs < 4; legs++) {
			if (reverse) {
				feet_pose_body[legs] = R_wb * feet_pose_inertia[legs]
						+ x_estimate_t.segment < 3 > (3);
			} else {
				feet_pose_body[legs] = R_wb.transpose()
						* (feet_pose_inertia[legs] - x_estimate_t.segment < 3
								> (3));
			}
		}
		return feet_pose_body;
	}
	actuated_joint_coordinates invkin(
			const FeetPositionDataMap feet_pose_body) const {
		actuated_joint_coordinates q;
		for (int legs = 0; legs < 4; legs++) {

			// step two, Inverse Kinematics calculate all actuated joint angle
			myInvkin->update(legs, feet_pose_body[legs]);
			Vector3d qi;
			myInvkin->getJointPosition(qi);
			q.segment < 3 > (legs * 3) = qi;

		}
		return q;
	}
	actuated_joint_coordinates invinstkin(
			const FloatingBaseFullState x_estimate_t,
			const actuated_joint_coordinates q) const {
		GeneralizedCoordinates bigq;
		actuated_joint_coordinates qdot;
		bigq << x_estimate_t.head(6), q;
		Jac->updateJacobians(bigq); // Get all jacobians

		FootVelocityVector foot_velocity;
		FootBaseJacobian Jb;
		FootJointJacobian Jr;
		for (int legs = 0; legs < 4; legs++) {
			foot_velocity.setZero();
			switch (legs) {
			case iit::hyq::LF:
				Jb = Jac->Jb_LF;
				Jr = Jac->Jr_LF;
				break;
			case iit::hyq::RF:
				Jb = Jac->Jb_RF;
				Jr = Jac->Jr_RF;
				break;
			case iit::hyq::LH:
				Jb = Jac->Jb_LH;
				Jr = Jac->Jr_LH;
				break;
			case iit::hyq::RH:
				Jb = Jac->Jb_RH;
				Jr = Jac->Jr_RH;
				break;
			}
			foot_velocity = foot_velocity - Jb * (x_estimate_t.tail(6)); // Minus influences from body

			qdot.segment < 3 > (legs * 3) = (Jr.inverse()) * foot_velocity;
		}
		return qdot;
	}
// inverse of jacobian gives the joint velocity

	Matrix3d GetBWrotation(FloatingBaseFullState x_est_t) {
		// Rotation transformation from base to Inertial frame
		Matrix3d R_wb;
		R_wb = Eigen::AngleAxisd(x_est_t(coordinates_order::qb2),
				Vector3d::UnitZ())
				* Eigen::AngleAxisd(x_est_t(coordinates_order::qb1),
						Vector3d::UnitY())
				* Eigen::AngleAxisd(x_est_t(coordinates_order::qb0),
						Vector3d::UnitX());

		return R_wb;
	}
	void ObserveRuntimeWeighting(Eigen::MatrixXcd Weighting,
			const FloatingBaseFullState x_est) const {
		std::cout << "At state " << x_est << std::endl;
		std::cout << "Current Jacobian" << Weighting;
	}
	void ObserveRuntimeResidual(const double* res) const {
		std::cout << "Runtime Residual: ";
		for (int i = 0; i < 54; i++) {
			std::cout << " ," << res[i];
			if (i % 12 == 11) {
				std::cout << " ||||";
			}
		}

		std::cout << " end\n";
	}
	void ObserveRuntimeParameter(const double* inertia,
			const double* feethold) const {
		std::cout << "Runtime Inertia Parameter: ";
		for (int i = 0; i < 10; i++) {
			std::cout << " ," << inertia[i];
		}
		std::cout << "\n Runtime Foothold Parameter: ";
		for (int i = 0; i < 12; i++) {
			std::cout << " ," << feethold[i];
		}

		std::cout << " end\n";
	}
private:
	const std::shared_ptr<InverseKinematics> myInvkin;
	const std::unique_ptr<HyQExtendedJacobians> Jac;
	const std::shared_ptr<Par_getter> par_getter;
	const std::shared_ptr<HyQKinematics> myHyQkin;
	const iit::hyq::LegDataMap<bool> feet_contact;
	const std::shared_ptr<HyQDynamics> dynamics;
	const Vector6d IMU_measure;
	const control_vector_t tau_measured;
	const actuated_joint_coordinates q_next;
	const actuated_joint_coordinates q_now;
	const double frequency;
	const double step_size;

// Measurement Noises Covariances
	Eigen::Matrix<double, 3, 3> Cov_foothold = 0.00025
			* Eigen::Matrix<double, 3, 3>::Identity(); // Slippage of foot is about 5 centimeters
	Eigen::Matrix<double, 12, 12> Cov_tau = 9
			* Eigen::Matrix<double, 12, 12>::Identity(); // Torque measurement covariance matrix 3N.m
	Eigen::Matrix<double, 3, 3> Cov_q = (0.000157 * 0.000157) // Corresponding to 1/80000 rotations
	* Eigen::Matrix<double, 3, 3>::Identity(); // Encoder measurement covariance matrix
	Eigen::Matrix<double, 12, 12> Cov_q_total = (0.000157 * 0.000157) // Corresponding to 1/80000 rotations
	* Eigen::Matrix<double, 12, 12>::Identity(); // Encoder measurement covariance matrix
	Eigen::Matrix<double, 3, 3> Cov_IMU_omega = 3E-4
			* Eigen::Matrix<double, 3, 3>::Identity(); // IMU angular velocity measurement covariance matrix
	Eigen::Matrix<double, 3, 3> Cov_IMU_acc = 4E-4 // 0.1 m/s² acceleration measurement noise
	* Eigen::Matrix<double, 3, 3>::Identity(); // IMU acceleration measurement covariance matrix
	Eigen::Matrix<double, 3, 3> Sigma_Process_orient = 7.6E-7 // Corresponding to 0.1 degrees
	* Eigen::Matrix<double, 3, 3>::Identity(); // Underlying Physics for transfering angular_velocity to position
	Eigen::Matrix<double, 3, 3> Sigma_Process_ang_vel = 3E-6 // Corresponding to 1 degrees/sec
	* Eigen::Matrix<double, 3, 3>::Identity(); // Underlying Physics for transfering angular_acc to angular_velocity
	Eigen::Matrix<double, 3, 3> Sigma_Process_pos = 1E-8
			* Eigen::Matrix<double, 3, 3>::Identity();
// Underlying Physics for transfering velocity to position. intergration error
	Eigen::Matrix<double, 3, 3> Sigma_Process_vel = 4E-6
			* Eigen::Matrix<double, 3, 3>::Identity();
// Underlying Physics for transfering acceleration to velocity. intergration error

	double eps = std::sqrt(Eigen::NumTraits<double>::epsilon()); // Nummeric Jacobian Calculation
}
;
