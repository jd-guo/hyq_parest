#include <iostream>
#include <ceres_tools/settings.h>

// This function helps setting up the calculation
void setup(char** argv){
	// Setup the methode in use
	std::string arg2 = argv[2];
	if (arg2 == "-b") {
		FULL_APPROACH = false;
	} else {
		FULL_APPROACH = true;
	}

	std::string arg3 = argv[3];
	if (arg3 == "-askin") {
		AS_KIN_STATE_ESTIMATOR = true;
		std::cout << "As kinematic Estimator, exclude dynamics.";
	} else if (arg3 == "-fix") {
		ESTIMATE_INERTIA_PARAMETERS = false;
		PERFECT_INITIALIZATION = true;
		std::cout << "Inertia parmater fixed at perfect, include dynamics.";
	}

	std::string arg4 = argv[4];
	if (arg4 == "-perfinertia") {
		PERFECT_INITIALIZATION = true;
	} else if (arg4 == "-perfstate") {
		PERFECT_STATE_INITIALIZE = true;}
	  else if (arg4 == "-closestate") {
		  CLOSE_INITIALIZE = true;
	} else if (arg4 == "-perf") {
		PERFECT_INITIALIZATION = true;
		PERFECT_STATE_INITIALIZE = true;
	}

	std::string arg5 = argv[5];
	if (arg5 == "-cov") {
		IDENTIFY_COVARIANCE = true;
	}

	std::string arg6 = argv[6];
	if (arg6 == "-obspar") {
		RUNTIME_OBSERVATION = true; num_thread = 1;
	} else if (arg6 == "-obsjac") {
		RUNTIME_JACOBIAN = true; num_thread = 1;
	}

}

void help_messages(){
	std::cerr
			<< "usage: hyq_parameter_estimation <pi_problem> type in all four argument t to use artificial test set\n";
	std::cerr << "first arguement select data, second baseline/full";
	std::cerr
			<< "third AsKin/FixedInertia/Full, forth none/inertia/state/both perfectly initialized, fifth covariance estimate \n";
	std::cerr << "-b :  Baseline Approach -f : Full Approach\n";
	std::cerr
			<< "-askin : As kinematic estimator  -fix : Fixed Inerita -full : Full Approach\n";
	std::cerr
			<< "-perfinertia : perfect inertia parameter initialized -perfstate: state initialized -perf:both  -none :none\n";
	std::cerr
			<< "-cov : Estimate with Covariance Weighting -nocov : Without \n";
	std::cerr
			<< "-obspar : Output runtime parameter and residuals -obsjac : Runtim Jacobian -none: none";

}
