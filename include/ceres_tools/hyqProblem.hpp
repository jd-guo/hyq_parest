#ifndef HYQPROBLEM_HPP_
#define HYQPROBLEM_HPP_
#endif

#include <memory>
#include <string>
#include <stdlib.h>
#include <time.h>
#include<iit/robots/hyq/link_data_map.h>
#include<ds_hyq_robot/extras/definitions.hpp>
#include<ds_hyq_robot/hyqDimensions.hpp>
#include<ds_hyq_robot/extras/kinematics/hyqKinematics.hpp>
#include <ds_hyq_robot/hyqDynamics.hpp>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <vector>
#include <iostream>
#include <Eigen/StdVector>
#include <cereal/archives/xml.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/Eigen.hpp>
#include <ceres_tools/settings.h>
#include <matlabCppInterface/MatFile.hpp>
#include "InverseKinematics.h"
#include <rosbag/bag.h>
#include <ceres_tools/settings.h>
#include <rosbag/view.h>
#include <boost/foreach.hpp>
#include "hyqb_msgs/ImuStamped.h"
#include "hyqb_msgs/JointStateStamped.h"
#include "hyqb_msgs/FloatingBaseStateStamped.h"
#include "hyqb_msgs/StateEstimate.h"
// For IMU dynamics, kindr implemented
#include <kindr/rotations/RotationEigen.hpp>
#define foreach BOOST_FOREACH
class hyqProblem {

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	typedef typename hyq::HyQDimensions::state_vector_t state_vector_t;
	typedef typename hyq::HyQDimensions::control_vector_t control_vector_t;
	typedef typename hyq::HyQDimensions::state_vector_array_t state_vector_array_t;
	typedef typename hyq::HyQDimensions::control_vector_array_t control_vector_array_t;
	typedef typename hyq::HyQDimensions::control_feedback_t control_feedback_t;
	typedef typename hyq::HyQDimensions::control_feedback_array_t control_feedback_array_t;
	typedef typename std::vector<GeneralizedCoordinates,
			Eigen::aligned_allocator<GeneralizedCoordinates> > GeneralizedCoordinates_array_t;

	typedef Eigen::Matrix<double, 6, 1> Vector6d;

	hyqProblem() {

		// This part initializes the parameters
		inertia_parameters_ = new double[10];
		*inertia_parameters_ = 30.0;
		for (int i = 1; i < 10; i++) {
			*(inertia_parameters_ + i) = 0.01;
		}
//		measure_offsets_ = new double[12];
//		for (int i = 0; i < 12; i++) {
//			*(measure_offsets_ + i) = 0;
//		}
		std::cout << "NOTION: the inertia parameters are initialized at:";
		for (int i = 0; i < 10; i++) {
			std::cout << inertia_parameters_[i] << ", ";
		}
		std::cout << "\n";
//		if (ESTIMATE_OFFSETS) {
//			std::cout << "NOTION: the measurement offsets are initialized at:";
//			for (int i = 0; i < 12; i++) {
//				std::cout << measure_offsets_[i] << ", ";
//			}
//			std::cout << "\n";
//		}
	}
	~hyqProblem() {
		delete[] inertia_parameters_; //Inertia parameter
		//delete[] measure_offsets_; // Torque measure offset
		//delete[] feethold_parameters_;
		delete[] state_estimate_;
		delete[] bias_estimate_;
		delete[] feethold_estimate_; //Foothold state, world
		big_q_traj.clear();
		qd_traj.clear();
		tau_measured_traj.clear();
		x_estimator_traj.clear();
		IMUmeasure_traj.clear();
		joint_angle_vel_traj.clear();
		joint_angle_traj.clear();
	}

	int num_observations() const {
		return num_observations_;
	}
	double step_time() const {
		return step_time_;
	}
	double total_time() const {
		return total_time_;
	}
	double* inertia_parameters() {
		return inertia_parameters_;
	} // all 10
//	double* feethold_parameters() {
//		return feethold_parameters_;
//	}
//	double* measure_offsets() {
//		return measure_offsets_;
//	}
	double* state_estimate(int i) {
		return state_estimate_ + 12 * i;
	}
	double* bias_estimate(int i) {
		return bias_estimate_ + 18 * i;
	}
	double* torque_bias_estimate(int i) {
		return bias_estimate_ + 18 * i + 6;
	}
	double* feethold_estimate(int i) {
		return feethold_estimate_ + 12 * i;
	}
	const actuated_joint_coordinates GetJointAngle(int i) {
		return joint_angle_traj.at(i);
	}
	const actuated_joint_coordinates GetJointAngleVelocity(int i) {
		return joint_angle_vel_traj.at(i);
	}
	const double* ground_truth_parameters() const {
		return ground_truth_parameters_;
	}
	void report() {
		std::cout << big_q_traj.size() << qd_traj.size()
				<< tau_measured_traj.size() << x_estimator_traj.size() << "\n";
	}
	const GeneralizedCoordinates GetQ(int i) const {
		return big_q_traj.at(i);
	}
	const GeneralizedCoordinates GetQd(int i) const {
		return qd_traj.at(i);
	}
	const control_vector_t GetTauMeasured(int i) const {
		return tau_measured_traj.at(i);
	}
	const state_vector_t GetXEstimator(int i) const {
		return x_estimator_traj.at(i);
	}
	const state_vector_array_t GetXEstimatortraj() const {
		return x_estimator_traj;
	}
	const Vector6d GetIMUmeasure(int i) const {
		return IMUmeasure_traj.at(i);
	}
	const Vector6d GetStateEstimate(int i) const {
		Vector6d x_estimate_t;
		x_estimate_t << *(state_estimate_ + 12 * i), *(state_estimate_ + 12 * i
				+ 1), *(state_estimate_ + 12 * i + 2), *(state_estimate_
				+ 12 * i + 3), *(state_estimate_ + 12 * i + 4), *(state_estimate_
				+ 12 * i + 5), *(state_estimate_ + 12 * i + 6), *(state_estimate_
				+ 12 * i + 7), *(state_estimate_ + 12 * i + 8), *(state_estimate_
				+ 12 * i + 9), *(state_estimate_ + 12 * i + 10), *(state_estimate_
				+ 12 * i + 11);
		std::cout << x_estimate_t << std::endl;
		return x_estimate_t;
	}
	const FeetPositionDataMap GetFootEstimate(int i) const {
		FeetPositionDataMap feethold_W;
		for (int legs = 0; legs < 4; legs++) {
			FootPositionVector pi_W;
			pi_W << *(feethold_estimate_ + 12 * i + 3 * legs), *(feethold_estimate_
					+ 12 * i + 3 * legs + 1), *(feethold_estimate_ + 12 * i
					+ 3 * legs + 2);
			feethold_W[legs] = pi_W;
		}
		return feethold_W;
	}
	void write_mat_from_bias_trajectory(
			std::vector<Eigen::Matrix<double, 18, 1> > bias_estimate_traj,
			char const* name) {
		std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> >
				IMU_angular_bias_traj, IMU_acc_bias_traj, Leg0_torque_bias_traj,
				Leg1_torque_bias_traj, Leg2_torque_bias_traj,
				Leg3_torque_bias_traj;
		std::cout << ">>>> Converting given bias traj to matlab file..."
				<< std::endl;

		for (int i = 0; i < num_observations_; i++) {
			Eigen::Matrix<double, 18, 1> bias_estimated = bias_estimate_traj.at(
					i);
			Eigen::Vector3d IMU_angular, IMU_acc, Leg0, Leg1, Leg2, Leg3;
			IMU_angular << bias_estimated.head(3);
			IMU_angular_bias_traj.push_back(IMU_angular);
			IMU_acc << bias_estimated.segment<3>(3);
			IMU_acc_bias_traj.push_back(IMU_acc);
			Leg0 << bias_estimated.segment<3>(6);
			Leg0_torque_bias_traj.push_back(Leg0);
			Leg1 << bias_estimated.segment<3>(9);
			Leg1_torque_bias_traj.push_back(Leg1);
			Leg2 << bias_estimated.segment<3>(12);
			Leg2_torque_bias_traj.push_back(Leg2);
			Leg3 << bias_estimated.tail(3);
			Leg3_torque_bias_traj.push_back(Leg3);

		}
		matlab::MatFile file;
		std::string matfileName("/home/jd/data/SmsProj/matlabfile/Bias_");
		matfileName += std::string(name);
		file.open(matfileName, matlab::MatFile::WRITE_COMPRESSED);
		assert(file.isOpen());
		assert(file.isWritable());
		file.put("IMU_angular_bias_traj", IMU_angular_bias_traj);
		file.put("IMU_acc_bias_traj", IMU_acc_bias_traj);
		file.put("Leg0_torque_bias_traj", Leg0_torque_bias_traj);
		file.put("Leg1_torque_bias_traj", Leg1_torque_bias_traj);
		file.put("Leg2_torque_bias_traj", Leg2_torque_bias_traj);
		file.put("Leg3_torque_bias_traj", Leg3_torque_bias_traj);
		file.close();

		std::cout << "The state trajectory is saved as matlab file under "
				<< matfileName << "\n";
	}

	void write_mat_from_state_trajectory(
			std::vector<Eigen::Matrix<double, 12, 1>> x_estimate_traj,
			char const* name) {

		std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> >
				Pos_estimate_traj, Orient_estimate_traj,
				Loc_angular_estimate_traj, Loc_vel_estimate_traj;
		std::cout << ">>>> Converting given state traj to matlab file..."
				<< std::endl;
		for (int i = 0; i < num_observations_; i++) {
			Eigen::Matrix<double, 12, 1> x_estimated = x_estimate_traj.at(i);
			Eigen::Vector3d orient, pos, loc_angular, loc_vel;
			orient << x_estimated.head(3);
			pos << x_estimated.segment<3>(3);
			loc_angular << x_estimated.segment<3>(6);
			loc_vel << x_estimated.tail(3);
			Orient_estimate_traj.push_back(orient);
			Pos_estimate_traj.push_back(pos);
			Loc_angular_estimate_traj.push_back(loc_angular);
			Loc_vel_estimate_traj.push_back(loc_vel);
		}
		matlab::MatFile file;
		std::string matfileName("/home/jd/data/SmsProj/matlabfile/");
		matfileName += std::string(name);
		file.open(matfileName, matlab::MatFile::WRITE_COMPRESSED);
		assert(file.isOpen());
		assert(file.isWritable());
		file.put("Orientation_Estimate_traj", Orient_estimate_traj);
		file.put("Position_Estimate_traj", Pos_estimate_traj);
		file.put("Loc_Angular_estimate_traj", Loc_angular_estimate_traj);
		file.put("Linear_vel_estimate_traj", Loc_vel_estimate_traj);
		file.close();

		std::cout << "The state trajectory is saved as matlab file under "
				<< matfileName << "\n";
	}

	void write_estimated_trajectory(char const* name) {
		std::vector<Eigen::Matrix<double, 12, 1>> x_estimate_traj;
		std::vector<Eigen::Matrix<double, 18, 1>> bias_estimate_traj;
		std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> >
				Pos_estimate_traj, Orient_estimate_traj,
				Loc_angular_estimate_traj, Loc_vel_estimate_traj;
		std::cout << ">>>> Writing estimated state" << std::endl;
		for (int i = 0; i < num_observations_; i++) {
			Eigen::Matrix<double, 12, 1> x_estimated;
			x_estimated << *(state_estimate_ + 12 * i), *(state_estimate_
					+ 12 * i + 1), *(state_estimate_ + 12 * i + 2), *(state_estimate_
					+ 12 * i + 3), *(state_estimate_ + 12 * i + 4), *(state_estimate_
					+ 12 * i + 5), *(state_estimate_ + 12 * i + 6), *(state_estimate_
					+ 12 * i + 7), *(state_estimate_ + 12 * i + 8), *(state_estimate_
					+ 12 * i + 9), *(state_estimate_ + 12 * i + 10), *(state_estimate_
					+ 12 * i + 11);
			x_estimate_traj.push_back(x_estimated);

			Eigen::Matrix<double, 18, 1> bias_estimated;
			bias_estimated << *(state_estimate_ + 18 * i), *(state_estimate_
					+ 18 * i + 1), *(state_estimate_ + 18 * i + 2), *(state_estimate_
					+ 18 * i + 3), *(state_estimate_ + 18 * i + 4), *(state_estimate_
					+ 18 * i + 5), *(state_estimate_ + 18 * i + 6), *(state_estimate_
					+ 18 * i + 7), *(state_estimate_ + 18 * i + 8), *(state_estimate_
					+ 18 * i + 9), *(state_estimate_ + 18 * i + 10), *(state_estimate_
					+ 18 * i + 11), *(state_estimate_ + 18 * i + 12), *(state_estimate_
					+ 18 * i + 13), *(state_estimate_ + 18 * i + 14), *(state_estimate_
					+ 18 * i + 15), *(state_estimate_ + 18 * i + 16), *(state_estimate_
					+ 18 * i + 17);
			bias_estimate_traj.push_back(bias_estimated);
		}
		write_mat_from_state_trajectory(x_estimate_traj, name);
		write_mat_from_bias_trajectory(bias_estimate_traj, name);
		{ // we need these brackets to make sure the archive goes out of scope and flushes
			std::ofstream outXML(
					"/home/jd/data/SmsProj/cereal/x_estimated.xml");
			cereal::XMLOutputArchive archive_o_xml(outXML);
			archive_o_xml(CEREAL_NVP(x_estimate_traj),
					CEREAL_NVP(bias_estimate_traj));

		}
	}
	bool LoadFile(const char* filename) {
		if (filename[0] == 't') {  // Use psyeudo file
			read_noisefree_data();
			SIMULATION_DATASET = false;
			return true;
		} else if (filename[0] == 's') {
			read_simulated_data();
			SIMULATION_DATASET = true;
			return true;
		} else {
			ConvertCerealToMatlab(filename);
			return false;

		}

	}

private:

	int num_observations_;
	double total_time_;
	double step_time_;
	double* inertia_parameters_;
//	double* feethold_parameters_;
	double * feethold_estimate_;
//	double* measure_offsets_;
	double* state_estimate_;
	double* bias_estimate_;
	std::vector<Eigen::Matrix<double, 12, 1> > floating_base_state_traj;
	std::vector<Eigen::Matrix<double, 12, 1> > kin_estimator_state_traj;
	GeneralizedCoordinates_array_t big_q_traj;
	GeneralizedCoordinates_array_t qd_traj;
	std::vector<Eigen::Matrix<double, 12, 1> > joint_angle_traj;
	std::vector<Eigen::Matrix<double, 12, 1> > joint_angle_vel_traj;
	control_vector_array_t tau_measured_traj;
	state_vector_array_t x_estimator_traj;
	std::vector<Vector6d> IMUmeasure_traj;
	double ground_truth_parameters_[10];
	void zero_initialization(int num_obs) {
		std::cout
				<< ">>>>Estimations are now initialized at zero, except the height...\n";
		state_estimate_ = new double[num_obs * 12];
		for (int i = 0; i < num_obs; i++) {

		for (int j = 0; j < 12; j++) {
			*(state_estimate_ + j +12 *i ) = initial_state[j];
		}
	}
		// Assigning some value to the base height to avoid issues at inverse kinematics
		bias_estimate_ = new double[num_obs * 18];
		feethold_estimate_ = new double[num_obs * 12];
	}

	void read_simulated_data() {
		std::cout << ">>>> Reading corrupted data from ros simulation... \n";

		rosbag::Bag bag;
		bag.open("/home/jd/data/SmsProj/GazeboData.bag", rosbag::bagmode::Read);
		std::vector<std::string> topics;

		topics.push_back(std::string("/hyqb/debug/sensors/joint_state")); // With Delay,joint state and torque
		topics.push_back(std::string("/hyqb/sensors/imu")); // IMU information
		topics.push_back(
				std::string("/hyqb/debug/sensors/floating_base_state")); // Real-time state of floating base
		topics.push_back(std::string("/hyqb/estimation/state")); // Kinematic Estimator
		rosbag::View view(bag, rosbag::TopicQuery(topics));
		kin_estimator_state_traj.resize(17000); // Change size account for missing data
		foreach(rosbag::MessageInstance const m,view)
		// Take values from sequential number 71355 until 86193
		{

			hyqb_msgs::JointStateStampedConstPtr joint_state_stamped =
					m.instantiate<hyqb_msgs::JointStateStamped>();
			if (joint_state_stamped != NULL) {
				if (joint_state_stamped->robot_header.seq >= 71355
						and joint_state_stamped->robot_header.seq <= 86193) {
//					std::cout
//							<< "  Loading Joint angle and torque information at time "
//							<< joint_state_stamped->robot_header.seq << "\n";
					Eigen::Matrix<double, 12, 1> joint_state_t(
							joint_state_stamped->joint_state.position.data());
					Eigen::Matrix<double, 12, 1> joint_torque_t(
							joint_state_stamped->joint_state.effort.data());
					Eigen::Matrix<double, 12, 1> joint_vel_t(
							joint_state_stamped->joint_state.velocity.data());
					tau_measured_traj.push_back(joint_torque_t);
					joint_angle_traj.push_back(joint_state_t);
					joint_angle_vel_traj.push_back(joint_vel_t);
				}
			}

			hyqb_msgs::ImuStampedConstPtr imu_stamped = m.instantiate<
					hyqb_msgs::ImuStamped>();

			if (imu_stamped != NULL) {
				if (imu_stamped->robot_header.seq >= 71355
						and imu_stamped->robot_header.seq <= 86193) {
//					std::cout << " Loading IMU information at time "
//							<< imu_stamped->robot_header.seq << "\n";
					Eigen::Matrix<double, 6, 1> IMU_data_t;
					IMU_data_t << imu_stamped->imu.angular_velocity.x, imu_stamped->imu.angular_velocity.y, imu_stamped->imu.angular_velocity.z, imu_stamped->imu.linear_acceleration.x, imu_stamped->imu.linear_acceleration.y, imu_stamped->imu.linear_acceleration.z;
					IMUmeasure_traj.push_back(IMU_data_t);
				}
			}

			hyqb_msgs::FloatingBaseStateStampedConstPtr fb_state_stamped =
					m.instantiate<hyqb_msgs::FloatingBaseStateStamped>();

			if (fb_state_stamped != NULL) {
				if (fb_state_stamped->robot_header.seq >= 71355
						and fb_state_stamped->robot_header.seq <= 86193) {
//					std::cout
//							<< " Loading Floating Base State information at time "
//							<< fb_state_stamped->robot_header.seq << "\n";
					Eigen::Matrix<double, 12, 1> floating_base_state_t;
					auto fb_st = fb_state_stamped->state; //
					kindr::rotations::eigen_impl::RotationQuaternionPD quat_t(
							fb_st.pose.orientation.w, fb_st.pose.orientation.x,
							fb_st.pose.orientation.y, fb_st.pose.orientation.z);
					kindr::rotations::eigen_impl::EulerAnglesXyzPD euler_t =
							kindr::rotations::eigen_impl::EulerAnglesXyzPD(
									quat_t);
					floating_base_state_t
							<< euler_t.getUnique().toImplementation(), fb_st.pose.position.x, fb_st.pose.position.y, fb_st.pose.position.z, // position in W
					fb_st.twist.angular.x, fb_st.twist.angular.y, fb_st.twist.angular.z, // Local angular
					fb_st.twist.linear.x, fb_st.twist.linear.y, fb_st.twist.linear.z; // local translation
					floating_base_state_traj.push_back(floating_base_state_t);
				}
			}
			hyqb_msgs::StateEstimateConstPtr kin_state = m.instantiate<
					hyqb_msgs::StateEstimate>();
			if (kin_state != NULL) {
				if (kin_state->robot_header.seq >= 71355
						and kin_state->robot_header.seq <= 86193) {
//					std::cout
//							<< " Loading Kinematic State Estimator information at time "
//							<< kin_state->robot_header.seq << "\n";
					Eigen::Matrix<double, 12, 1> kin_state_t;
					auto kin_st = kin_state->base; //
					Eigen::Matrix<double, 3, 1> axis(kin_st.pose.orientation.x,
							kin_st.pose.orientation.y,
							kin_st.pose.orientation.z);
					kindr::rotations::eigen_impl::RotationQuaternionPD quat_t(
							kin_st.pose.orientation.w,
							kin_st.pose.orientation.x,
							kin_st.pose.orientation.y,
							kin_st.pose.orientation.z);
					kindr::rotations::eigen_impl::EulerAnglesXyzPD euler_t =
							kindr::rotations::eigen_impl::EulerAnglesXyzPD(
									quat_t);
					kin_state_t << euler_t.getUnique().toImplementation(), kin_st.pose.position.x, kin_st.pose.position.y, kin_st.pose.position.z, // position in W
					kin_st.twist.angular.x, kin_st.twist.angular.y, kin_st.twist.angular.z, // Local angular
					kin_st.twist.linear.x, kin_st.twist.linear.y, kin_st.twist.linear.z; // local translation
//					kin_estimator_state_traj.push_back(kin_state_t);
					kin_estimator_state_traj.at(
							kin_state->robot_header.seq - 71355) = kin_state_t;
				}
			}

		}

		num_observations_ = joint_angle_traj.size();
		std::cout << "SUCESS: Loaded number of messages: "
				<< joint_angle_traj.size() << std::endl;
		zero_initialization(num_observations_);
		step_time_ = 0.001;
		total_time_ = double(num_observations_) * step_time_;

// Initialize the state perfectly
		if (PERFECT_STATE_INITIALIZE) {
			std::cout
					<< "NOTION: the state estimation are initialized to be perfect \n";
			for (int i = 0; i < num_observations_; i++) {
				for (int j = 0; j < 12; j++) {
					*(state_estimate_ + 12 * i + j) =
							floating_base_state_traj.at(i)(j); // This is orientation and position
				}
			}

			for (int i = 0; i < num_observations_; i++) {
				for (int j = 0; j < 18; j++) {
					*(bias_estimate_ + 18 * i + j) = 0;
				}

			}
		}

//  Initialize reasonably well
if (CLOSE_INITIALIZE) {
	std::cout
			<< "NOTION: the state estimation are initialized from kin. Estimator \n";

	for (int i = 0; i < num_observations_; i++) {
		for (int j = 0; j < 12; j++) {
			*(state_estimate_ + 12 * i + j) =
						kin_estimator_state_traj.at(i)(j) ; // This is orientation and position
		}
	}

	for (int i = 0; i < num_observations_; i++) {
		for (int j = 0; j < 18; j++) {
			*(bias_estimate_ + 18 * i + j) = 0;
		}

	}
}
//Hard coded in total time
		total_time_ = 12.0;

		double true_parameters[] = { 64.862, 0.0272220, -0.00002350, 0.042224,
				1.688238, 8.665299, 9.243560, -0.028789, 0.277694, 0.00382 };

		for (int i = 0; i < 10; ++i) {
			ground_truth_parameters_[i] = true_parameters[i];
		}

// Initialize perfect inertia parameters
		if (PERFECT_INITIALIZATION) {
			for (int i = 0; i < 10; i++) {
				*(inertia_parameters_ + i) = true_parameters[i];
			}
			std::cout
					<< "NOTION: the inertia parameters are perfect re-initialized at:";
			for (int i = 0; i < 10; i++) {
				std::cout << inertia_parameters_[i] << ", ";
			}
		}
		HyQKinematics myHyQkin;

// Initialize average feethold location
		FeetPositionDataMap feethold_W;
		FeetPositionDataMap feethold_B;
		FeetPositionDataMap feethold_W_average;
//		FeetPositionDataMap feethold_B_average;
		for (int i = 0; i < num_observations_; i++) {
			GeneralizedCoordinates big_q;
			GeneralizedCoordinates qd;
			state_vector_t x_estimate_t;
			x_estimate_t << kin_estimator_state_traj.at(i).head(6), Eigen::Matrix<
					double, 12, 1>::Zero(), kin_estimator_state_traj.at(i).tail(
					6), Eigen::Matrix<double, 12, 1>::Zero();
			big_q << floating_base_state_traj.at(i).head(6), joint_angle_traj.at(
					i);
			qd << floating_base_state_traj.at(i).tail(6), joint_angle_vel_traj.at(
					i);
			big_q_traj.push_back(big_q);
			qd_traj.push_back(qd);
			x_estimator_traj.push_back(x_estimate_t);
			myHyQkin.GetFeetPose(big_q, feethold_W, feethold_B);

			//Properly initialize all foothold position

			for (int j = 0; j < 4; j++) {
				*(feethold_estimate_ + 3 * j + 12 * i) = feethold_W[j](0);
				*(feethold_estimate_ + 3 * j + 12 * i + 1) = feethold_W[j](1);
				*(feethold_estimate_ + 3 * j + 12 * i + 2) = feethold_W[j](2);
				feethold_W_average[j] += feethold_W[j];
//				feethold_B_average[j] += feethold_B[j];
			}

		}
		if (PERFECT_FEET_INITIALIZE) {
			std::cout
					<< "\nNOTION: The foothold positions are initialized at the perfect position:";

			std::cout
					<< "Assuming constant Foothold, initialize at the average and setting foot 4 on the ground... \n ";
			for (int j = 0; j < 3; j++) {
				*(feethold_estimate_ + 3 * j) = feethold_W_average[j](0)
						/ (double) num_observations_;
				*(feethold_estimate_ + 3 * j + 1) = feethold_W_average[j](1)
						/ (double) num_observations_;
				*(feethold_estimate_ + 3 * j + 2) = feethold_W_average[j](2)
						/ (double) num_observations_;
			}
			for (int j = 0; j<3 ; j++){
				*(feethold_estimate_ + 9 + j ) = fixed_foot_pos[j];
			}
			for (int i = 0; i < 12; i++) {
				std::cout << " " << feethold_estimate_[i];
			}
			std::cout << "\n";
		}
		write_mat_from_state_trajectory(floating_base_state_traj,
				"GroundTruthStateGazebo.mat");
		write_mat_from_state_trajectory(kin_estimator_state_traj,
				"EstimatedKin.mat");
	}

	void ConvertCerealToMatlab(const char* filename) { // Convert saved XML file into matlab
		std::vector<Eigen::Matrix<double, 12, 1> > FullBodyState_traj;
		std::cout << ">>>> Reading estimated states from " << filename
				<< std::endl;

		// Start reading using r_cereal
		std::ifstream inXML_i(filename);
		cereal::XMLInputArchive archive_i_xml(inXML_i);
		archive_i_xml(cereal::make_nvp("x_estimate_traj", FullBodyState_traj));
		num_observations_ = FullBodyState_traj.size();
		write_mat_from_state_trajectory(FullBodyState_traj,
				"Estimated_FixedInertia.mat");
	}
	void read_noisefree_data() {

		std::cout
				<< ">>>> Reading noisefree data from /home/jd/data/SmsProj ..."
				<< std::endl;
		// Start reading using r_cereal

		std::ifstream inXML_i("/home/jd/data/SmsProj/controlApplied.xml");
		cereal::XMLInputArchive archive_i_xml(inXML_i);
		archive_i_xml(cereal::make_nvp("u_applied", tau_measured_traj));
		std::ifstream inXML_ii("/home/jd/data/SmsProj/trajectory.xml");
		cereal::XMLInputArchive archive_ii_xml(inXML_ii);
		archive_ii_xml(cereal::make_nvp("x_rollout", x_estimator_traj));

		// Initialize state estimation
		num_observations_ = tau_measured_traj.size();
		zero_initialization(num_observations_);
		for (int i = 0; i < num_observations_; i++) {
			Eigen::Matrix<double, 12, 1> joint_angle_t =
					x_estimator_traj.at(i).segment<12>(6);
			joint_angle_traj.push_back(joint_angle_t);
			Eigen::Matrix<double, 12, 1> joint_angle_vel_t =
					x_estimator_traj.at(i).tail(12);
			joint_angle_vel_traj.push_back(joint_angle_vel_t);
		}

		// Initialize state to be perfects
		if (PERFECT_STATE_INITIALIZE) {
			std::cout
					<< "NOTION: the state estimation are initialized to be perfect \n";
			for (int i = 0; i < num_observations_; i++) {
				for (int j = 0; j < 6; j++) {
					*(state_estimate_ + 12 * i + j) = x_estimator_traj.at(i)(j); // This is orientation and position
					*(state_estimate_ + 12 * i + j + 6) =
							x_estimator_traj.at(i)(j + 18); // local angular velocity and velocity
				}
			}
		}
		//  Initialize reasonably well
		if (CLOSE_INITIALIZE) {
			std::cout
					<< "NOTION: the state estimation are initialized close from perfect \n";
			srand(time(NULL));
			for (int i = 0; i < num_observations_; i++) {
				for (int j = 0; j < 6; j++) {
					*(state_estimate_ + 12 * i + j) = x_estimator_traj.at(i)(j) +rand()%1 * 0.01 ; // This is orientation and position
					*(state_estimate_ + 12 * i + j + 6) =
							x_estimator_traj.at(i)(j + 18)+rand()%1*0.05; // local angular velocity and velocity
				}
			}

			for (int i = 0; i < num_observations_; i++) {
				for (int j = 0; j < 18; j++) {
					*(bias_estimate_ + 18 * i + j) = 0;
				}

			}
		}
		// Initialize bias estimation
		for (int i = 0; i < num_observations_; i++) {
			for (int j = 0; j < 18; j++) {
				*(bias_estimate_ + 18 * i + j) = 0;
			}

		}
		//Hard coded in total time
		total_time_ = 12.0;

		double true_parameters[] = { 47.376, -0.02230, -0.00010, 0.03870,
				1.209488, 5.5837, 6.056973, 0.00571, -0.190812, -0.012668 };

		for (int i = 0; i < 10; ++i) {
			ground_truth_parameters_[i] = true_parameters[i];
		}

		// Initialize perfect inertia parameters
		if (PERFECT_INITIALIZATION) {
			for (int i = 0; i < 10; i++) {
				*(inertia_parameters_ + i) = true_parameters[i];
			}

			std::cout
					<< "NOTION: the inertia parameters are re-initialized at:";
			for (int i = 0; i < 10; i++) {
				std::cout << inertia_parameters_[i] << ", ";
			}
		}
		HyQKinematics myHyQkin;

		// Extra Step: Overwriting all q,qd,and IMU measure with x_estimator
		// Adding Artificial IMU Data
		Vector6d IMU_measure; // Bodyframe acceleration, local angular velocity

		step_time_ = total_time_ / (double) num_observations_;
		FeetPositionDataMap feethold_W;
		FeetPositionDataMap feethold_B;

		FeetPositionDataMap feethold_W_average;
//		FeetPositionDataMap feethold_B_average;
		for (int i = 0; i < num_observations_; i++) {
			GeneralizedCoordinates big_q;
			GeneralizedCoordinates qd;

			myHyQkin.hyqconv_->GeneralizedCoordinatesFromStateVector(
					x_estimator_traj.at(i), big_q, qd);
			myHyQkin.GetFeetPose(big_q, feethold_W, feethold_B);

			//Properly initialize all foothold position

			for (int j = 0; j < 4; j++) {
				*(feethold_estimate_ + 3 * j + 12 * i) = feethold_W[j](0);
				*(feethold_estimate_ + 3 * j + 12 * i + 1) = feethold_W[j](1);
				*(feethold_estimate_ + 3 * j + 12 * i + 2) = feethold_W[j](2);
				feethold_W_average[j] += feethold_W[j];
//				feethold_B_average[j] += feethold_B[j];
			}

			big_q_traj.push_back(big_q);
			qd_traj.push_back(qd);

			// Properly initialize the foothold position
			if (i == 0) {
			}

			else { // artificial IMU data
				for (int j = 0; j < 4; j++) {

				}
				state_vector_t Diff = (x_estimator_traj.at(i)
						- x_estimator_traj.at(i - 1)) / step_time_;

				Vector6d IMU_measure;
				IMU_measure << x_estimator_traj.at(i)(18), x_estimator_traj.at(
						i)(19), x_estimator_traj.at(i)(20), Diff(21), Diff(22), Diff(
						23);
				// Assign local angular velocity and linearized acceleration
				IMUmeasure_traj.push_back(IMU_measure);

			}

		}

		if (PERFECT_FEET_INITIALIZE) {
			std::cout
					<< "\nNOTION: The foothold positions are initialized at the perfect position:";

			std::cout
					<< "Assuming constant Foothold, initialize at the average and setting foot 4 on the ground... \n ";
			for (int j = 0; j < 3; j++) {
				*(feethold_estimate_ + 3 * j) = feethold_W_average[j](0)
						/ (double) num_observations_;
				*(feethold_estimate_ + 3 * j + 1) = feethold_W_average[j](1)
						/ (double) num_observations_;
				*(feethold_estimate_ + 3 * j + 2) = feethold_W_average[j](2)
						/ (double) num_observations_;
			}
			for (int j = 0; j<3 ; j++){
				*(feethold_estimate_ + 9 + j ) = fixed_foot_pos[j];
			}
			for (int i = 0; i < 12; i++) {
				std::cout << " " << feethold_estimate_[i];
			}
			std::cout << "\n";
		}

		std::cout << "SUCCESS:  Noisefree data loaded. In total:  "
				<< num_observations_ << " observations" << "\n";

	}
};
