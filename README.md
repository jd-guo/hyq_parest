# README #
This is the work of my semester project in Summer 2016, at adrl of ETH under supervision from Michael Neunert.

### Prerequisite
Requires ROS and catkin tool.

Public Package required:  ceres, Eigen, kindr 0.0.1

Institute Package required: c_dynamical_system, r_cereal, ct_core, ds_hyq_robot, hyq_inverse_kinematics, matlab_cpp_interface, hyqb_msgs.


The report comes along with the semester project is available at
https://www.overleaf.com/read/jnczdkwvsxcc.

### What is this repository for? ###
This repository contains a numeric tool to determine the inertia parameter of hyQ.
In the expanded method, it estimates the state of the robot at the same time.


### How do I get set up? ###
First, clone all repositories from institute bitbucket page.

In catkin_make environment, build the package. 

execute function like this:
rosrun hyq_parest hyq_parest 't' -b -n -n -n -n 
for noise-free test data using simple approach

rosrun hyq_parest hyq_parest 's' -f -full -none -cov -none 
for simulated example data using full approach and covariance matrix




### Who do I talk to? ###

Work from Jiadong Guo under supervisor Michael Neunert. 

Implementation follows hyqblue_tools from Diego Pardo.