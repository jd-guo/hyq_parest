#include <iostream>
#include <cstdio>
#include <ceres_tools/Setup.hpp>
#include "ceres/ceres.h"
#include "glog/logging.h"
#include <ceres_tools/settings.h>
#include <ceres_tools/hyqNumDiff.hpp>
#include <ceres_tools/hyqFullNumDiff.hpp>
#include <ceres_tools/hyqProblem.hpp>
#include <Eigen/Dense>

#include <gperftools/profiler.h>

int main(int argc, char** argv) {
	google::InitGoogleLogging(argv[0]);
	if (argc != 7) {
		help_messages();
	return 1;
	}

	setup(argv);
	// Setup variables and build up test
	hyqProblem hyqproblem;
	// Initialize parameter id Problem
	// including Load Data and save all measurements

	if (argv[1][0] == 's') {integrationRate = 1000;} // Adjust intergration step

	if (!hyqproblem.LoadFile(argv[1])) {
		std::cerr << "ERROR: unable to open file " << argv[1] << "\n";
		return 1;
	}

	const double SAMPLING_DIFF = hyqproblem.step_time();

	//-------------------------------------------------Set up residual blocks-------------------------------
	ceres::Problem problem;

	std::cout << ">>>> Add in Residual blocks..." << "\n";

	int counter = 0;
	const double TIME_STEP = SAMPLING_DIFF;
	const int frequency = integrationRate;
	std::cout << "NOTION: Time step set to be " << TIME_STEP << "\n";

	// Setup Ceres stricture for full approach
	if (FULL_APPROACH) {
		for (int i = 0; i < hyqproblem.num_observations() - 1; i = i + 1) {
			ceres::CostFunction* cost_function = hyqFullNumDiffResidual::Create(
					hyqproblem.GetIMUmeasure(i), hyqproblem.GetTauMeasured(i),
					hyqproblem.GetJointAngle(i),
//									hyqproblem.GetJointAngleVelocity(i),
					hyqproblem.GetJointAngle(i + 1),

					TIME_STEP

					);
			problem.AddResidualBlock(cost_function, NULL,
					hyqproblem.inertia_parameters(),
					hyqproblem.feethold_estimate(0),
					hyqproblem.bias_estimate(i),
					hyqproblem.bias_estimate(i + 1),
					hyqproblem.state_estimate(i),
					hyqproblem.state_estimate(i + 1));
			problem.SetParameterLowerBound(hyqproblem.state_estimate(i),5,0.05);
			problem.SetParameterUpperBound(hyqproblem.state_estimate(i),5,0.7);

			for (int i = 0; i < 3; i++) {
				// Estimated feethold over zero
				problem.SetParameterLowerBound(hyqproblem.inertia_parameters(),
						4 + i, 0);
				// Constrain the diagonal elements to be positive, important for covergence
			}
			counter = counter + 1;
		}
	} else {  // Setup Ceres stricture for full approach
		int SKIP_STEP = 1;
		for (int i = 0; i < hyqproblem.num_observations() - 1;
				i = i + SKIP_STEP) {
			ceres::CostFunction* cost_function = hyqNumDiffResidual::Create(
					hyqproblem.GetXEstimator(i),
					hyqproblem.GetXEstimator(i + 1),
					hyqproblem.GetTauMeasured(i), hyqproblem.GetQ(i),
					hyqproblem.GetQd(i), TIME_STEP

					);
			problem.AddResidualBlock(cost_function, NULL,
					hyqproblem.inertia_parameters(),
					hyqproblem.torque_bias_estimate(i),
					hyqproblem.torque_bias_estimate(i + 1));

			counter = counter + 1;
		}

	}

	std::cout << "SUCCESS: All Residual Blocks added, in total: " << counter
			<< " Blocks\n";

	// -----------------------------------------Start the Solver----------------------------------
	ceres::Solver::Options options;
	options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
	options.minimizer_progress_to_stdout = true;
	options.num_threads = num_thread;
	options.function_tolerance = 1e-6; // Instead of 1e-6, terminate earlier
	//options.max_num_iterations = 20;
	ceres::Solver::Summary summary;
	if (FULL_APPROACH) {
		std::cout << "ATTENTION: USING FULL APPROACH \n";
	} else {
		std::cout << "ATTENTION: USING BASELINE APPROACH \n";
	}

	std::cout << ">>>> Solving non-linear least squares problem..." << "\n";
	std::cout << ">>>> with frequency=" << frequency << "Hz, constant torque "
			<< "over each time step between " << 2
			<< " states, Euler integrates " << TIME_STEP * frequency
			<< " steps. \n";

	if (IDENTIFY_COVARIANCE) {
		std::cout
				<< "ATTENTION: Using nummeric jacobian to calculate covariance matrix. This can take long.\n";
	}

	// Start the profiler
	ProfilerStart("PjDynamics.prof");
	ceres::Solve(options, &problem, &summary);
	ProfilerStop();


	// Output on screen
	std::cout << summary.FullReport() << "\n";

	// The parameters found
	std::cout << "RESULT: the inertia parameters are:";
	for (int i = 0; i < 10; i++) {
		std::cout << " " << (hyqproblem.inertia_parameters())[i] << "("
				<< (hyqproblem.ground_truth_parameters())[i] << ") ";
	}
	std::cout << "\n";

	// The identified foothold location and save state estimation
	if (FULL_APPROACH) {
		std::cout << "RESULT: the Foothold parameters are:";
		for (int i = 0; i < 12; i++) {
			std::cout << " " << (hyqproblem.feethold_estimate(0))[i];
		}
		std::cout << "\n";
		hyqproblem.write_estimated_trajectory(CurrentName);
	} else {
		if (ESTIMATE_BIAS) {
			hyqproblem.write_estimated_trajectory(CurrentName);
		} // Torque bias trajectory
	}

	return 0;

}
